﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAnimator : MonoBehaviour
{
 
    public Animator Aim;
    // Start is called before the first frame update
    void Start()
    {
        Aim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(3) == true)
        {
                 Aim.SetBool("Aiming", true);

        }else{  Aim.SetBool("Aiming", false);    }

    }
}

  
