﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private Rigidbody2D bulltRB;
    public float bulletSpeed;

    public int damage;
    // Start is called before the first frame update
    private void Awake()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform != this.transform && !collision.CompareTag("MainCharacter") && !collision.CompareTag("EnemyBullet") && !collision.CompareTag("PlayerBulletHitbox") && !collision.CompareTag("PlayerBullet") && !collision.CompareTag("InvisibleTrigger"))
        {
            collision.SendMessage("TakeDamage ", damage, SendMessageOptions.DontRequireReceiver);
            Destroy(gameObject);
        }
    }
}
