﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public Transform spawner;
    public Transform spawner2;
    public Transform spawner3;
    public Transform spawner4;
    public Transform spawner5;
    public GameObject prefap;

    public Text currentMunicion;

    // Start is called before the first frame update
    float lookAngel;
    Vector2 lookDirection;
    float bulletSpeed = 10;
    bool canShoot = true;
    float nextFire = 0;
    public static int weapon = 1;
    public int weapon_suplementaria;
    public static bool havePistol = true;
    public static bool haveShotgun = false;
    public static bool haveRifle = false;
    int CurrentPistol, CurrentShotgun, CurrentAuto;
    int MaxMPistol = 7, MaxMShotgun = 4, MaxMAuto = 15;

    void Start()
    {
        CurrentPistol=MaxMPistol; CurrentShotgun= MaxMShotgun; CurrentAuto= MaxMAuto;
        lookDirection = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        lookAngel = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
    }

    // Update is called once per frame
    void Update()
    {
        shoot();


        weapon_suplementaria = weapon;


        reload();

        if (Input.GetKeyDown(KeyCode.Q) && haveShotgun == true && weapon == 1)
        {
            SoundManagerScript.PlaySound("changeWeapon");
            weapon++;

            

        }

        else if (Input.GetKeyDown(KeyCode.Q) && haveRifle == false && haveShotgun == true && weapon == 2)
        {
            SoundManagerScript.PlaySound("changeWeapon");
            Debug.Log("Volviste a pistola desde escopeta " + weapon);
            weapon = 1;
        }

        else if (Input.GetKeyDown(KeyCode.Q) && haveShotgun == true && haveRifle == true && weapon == 2)
        {
            SoundManagerScript.PlaySound("changeWeapon");
            weapon++;

           
        }


        else if (Input.GetKeyDown(KeyCode.Q) && weapon == 3)
        {
            SoundManagerScript.PlaySound("changeWeapon");
            weapon = 1;

            
        }

        
    }

    public void shoot()
    {
        switch (weapon)
        {
            case 1:
                currentMunicion.text=""+CurrentPistol;
                pistol();
                break;
            case 2:
                currentMunicion.text = "" + CurrentShotgun;
                shotgun();
                break;
            case 3:
                currentMunicion.text = "" + CurrentAuto;
                autoRifle();
                break;
        }
    }
    public void pistol()
    {
        if (Input.GetButton("Fire1") && Time.time > nextFire&&CurrentPistol!=0)
        {
            nextFire = 0.35f + Time.time;
            GameObject bullet = Instantiate(prefap, spawner.position, Quaternion.Euler(0, 0, lookAngel));
            bullet.GetComponent<Rigidbody2D>().velocity = spawner.right * bulletSpeed;
            SoundManagerScript.PlaySound("playerShot");
            CurrentPistol--;

            if(CurrentPistol == 0)
            {
                SoundManagerScript.PlaySound("needToReload");

            }
        }
    }
    public void shotgun()
    {
        if (Input.GetButtonDown("Fire1") && Time.time > nextFire && CurrentShotgun!=0)
        {
            nextFire = 1.5f + Time.time;
            GameObject bullet1 = Instantiate(prefap, spawner.position, Quaternion.Euler(0, 0, lookAngel));
            bullet1.GetComponent<Rigidbody2D>().velocity = spawner.right * bulletSpeed;
            GameObject bullet2 = Instantiate(prefap, spawner2.position, Quaternion.Euler(0, 0, lookAngel));
            bullet2.GetComponent<Rigidbody2D>().velocity = spawner2.right * bulletSpeed;
            GameObject bullet3 = Instantiate(prefap, spawner3.position, Quaternion.Euler(0, 0, lookAngel));
            bullet3.GetComponent<Rigidbody2D>().velocity = spawner3.right * bulletSpeed;
            GameObject bullet4 = Instantiate(prefap, spawner4.position, Quaternion.Euler(0, 0, lookAngel));
            bullet4.GetComponent<Rigidbody2D>().velocity = spawner4.right * bulletSpeed;
            GameObject bullet5 = Instantiate(prefap, spawner5.position, Quaternion.Euler(0, 0, lookAngel));
            bullet5.GetComponent<Rigidbody2D>().velocity = spawner5.right * bulletSpeed;
            SoundManagerScript.PlaySound("shotgunShot");
            CurrentShotgun--;


            if (CurrentShotgun == 0)
            {
                SoundManagerScript.PlaySound("needToReload");

            }
        }
    }
    public void autoRifle()
    {
        if (Input.GetButton("Fire1") && canShoot && CurrentAuto!=0)
        {
            canShoot = false;
            GameObject bullet = Instantiate(prefap, spawner.position, Quaternion.Euler(0, 0, lookAngel));
            bullet.GetComponent<Rigidbody2D>().velocity = spawner.right * bulletSpeed;
            StartCoroutine(autoCorrutine(0.15f));
            SoundManagerScript.PlaySound("rifleShot");
            CurrentAuto--;


            if (CurrentAuto == 0)
            {
                SoundManagerScript.PlaySound("needToReload");

            }
        }
    }
    IEnumerator autoCorrutine(float time)
    {
        canShoot = false;
        yield return new WaitForSeconds(time);
        canShoot = true;
    }
    public void reload()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {

            switch (weapon)
            {
                case 1:
                    CurrentPistol = MaxMPistol;
                    break;
                case 2:
                    CurrentShotgun = MaxMShotgun;
                    break;
                case 3:
                    CurrentAuto = MaxMAuto;
                    break;
            }
        }
    }
}
