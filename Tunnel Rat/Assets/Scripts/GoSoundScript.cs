﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoSoundScript : MonoBehaviour
{
    public bool ithappened = false;


    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("PlayerBulletHitbox") && ithappened == false)
        {
            SoundManagerScript.PlaySound("goSound");
            ithappened = true;
        }
    }


}
