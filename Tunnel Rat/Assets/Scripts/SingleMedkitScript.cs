﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleMedkitScript : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("PlayerBulletHitbox") && GameManagerScript.health < 3)
        {
            SoundManagerScript.PlaySound("healthUp");
            GameManagerScript.health += 1;
            Destroy(gameObject);
        }
    }
}
