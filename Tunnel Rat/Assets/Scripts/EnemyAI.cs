﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public Transform[] patrolPoints;
    public float speed;
    Transform currentPatrolPoint;
    int currentPatrolIndex;

    public static int arenaEnemiesKilled = 0;

    Animator walkingAnimator; 

    public Transform target;
    public float chaseRange;

    public float attackRange;
    public int damage;
    private float lastAttackTime;
    public float attackDelay;

    public GameObject projectile;
    public float EnemyBulletForce;

    public int vida;
    public bool movimiento = false;

    private Material matWhite;
    private Material matDefault;
    SpriteRenderer sr;
    private UnityEngine.Object explosionRef;

    // Start is called before the first frame update
    void Start()
    {
        currentPatrolIndex = 0;
        currentPatrolPoint = patrolPoints[currentPatrolIndex];

        sr = GetComponent<SpriteRenderer>();
        matWhite = Resources.Load("WhiteFlash", typeof(Material)) as Material;
        matDefault = sr.material;
        explosionRef = Resources.Load("Explosion");
        walkingAnimator = transform.GetChild(1).GetComponent<Animator>();
        //Debug.Log(transform.GetChild(1).gameObject.name);
    }

    // Update is called once per frame
    void Update()
    {


        float distanceToPlayer = Vector3.Distance(transform.position, target.position);
  
        if (distanceToPlayer > attackRange && distanceToPlayer < chaseRange)
        {
            //Empezar a perseguir al target - girar hacia el
            Vector3 targetDir = target.position - transform.position;
            float angle2 = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90f;
            Quaternion q2 = Quaternion.AngleAxis(angle2, Vector3.forward);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, q2, 180);

            //Raycast para ver si tiene vision del target
            RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, chaseRange);
            //Checkear si tiene vision de algo, i ver de que
            if (hit.transform == target)
            {
                transform.Translate(Vector3.up * Time.deltaTime * speed);
                movimiento = true;
            }
        }
        else
        {

            if (distanceToPlayer <= attackRange)
            {
                //Girar hacia el target
                Vector3 targetDir = target.position - transform.position;
                float angle3 = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90f;
                Quaternion q3 = Quaternion.AngleAxis(angle3, Vector3.forward);
                //este tiene un Time.deltaTime porque queremos que gire a una velocidad limitada cuando ataca,
                // que sus tiros no sean como rayos laser
                transform.rotation = Quaternion.RotateTowards(transform.rotation, q3, 110 * Time.deltaTime);
                //Mirar si es la hora de atacar
                if (Time.time > lastAttackTime + attackDelay)
                {
                    //Raycast para ver si tiene vision del target
                    RaycastHit2D hit = Physics2D.Raycast(transform.position, transform.up, attackRange);
                    //Checkear si tiene vision de algo, i ver de que
                    if (hit.transform == target)
                    {
                        //Ve al jugador - disparar el proyectil
                        GameObject newEnemyBullet = Instantiate(projectile, transform.position, transform.rotation);
                        newEnemyBullet.GetComponent<Rigidbody2D>().AddRelativeForce(new Vector2(0f, EnemyBulletForce));
                        SoundManagerScript.PlaySound("enemyShot");
                        lastAttackTime = Time.time;
                        movimiento = false;
                    }
                }
            }
        }
        walkingAnimator.SetBool("movimiento", movimiento);

    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerBullet"))
        {
            vida--;
            chaseRange = 100;
            sr.material = matWhite;
            SoundManagerScript.PlaySound("enemyHit");
            if (vida == 0)
            {
                SoundManagerScript.PlaySound("enemyDie");
                GameObject explosion = (GameObject)Instantiate(explosionRef);
                explosion.transform.position = new Vector3(transform.position.x, transform.position.y + .3f, transform.position.z);
                arenaEnemiesKilled++;
                Destroy(collision.gameObject);
                Destroy(gameObject);
            }
            else
            {
                Invoke("ResetMaterial", .1f);
            }
        }
    }
    void ResetMaterial()
    {
        sr.material = matDefault;
    }

}


