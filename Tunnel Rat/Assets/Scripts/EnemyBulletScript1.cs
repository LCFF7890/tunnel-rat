﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletScript1 : MonoBehaviour
{
    public int damage;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform != this.transform && !collision.CompareTag("Enemy") && !collision.CompareTag("EnemyBullet") && !collision.CompareTag("PlayerBullet") && !collision.CompareTag("Player") && !collision.CompareTag("EnemyWall") && !collision.CompareTag("InvisibleTrigger"))
        {
            if (collision.CompareTag("PlayerBulletHitbox"))
            {
                SoundManagerScript.PlaySound("heroScream");
                GameManagerScript.health -= 1;
                Destroy(gameObject);

            }
            else
            {
                Destroy(gameObject);

            }
        }
    }

}
