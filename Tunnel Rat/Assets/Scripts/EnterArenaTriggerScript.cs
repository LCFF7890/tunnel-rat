﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterArenaTriggerScript : MonoBehaviour
{

    public static bool laHePisado = false;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerBulletHitbox"))
        {
            laHePisado = true;
            SoundManagerScript.PlaySound("arenaAlarm");
            Destroy(gameObject);


        }
    }
}
