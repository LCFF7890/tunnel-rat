﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotShotgun : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerBulletHitbox"))
        {
            SoundManagerScript.PlaySound("changeWeapon");
            Spawner.haveShotgun = true;
            Destroy(gameObject);

        }

    }
}
