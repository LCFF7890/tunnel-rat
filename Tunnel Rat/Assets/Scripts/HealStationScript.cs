﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealStationScript : MonoBehaviour
{
   void OnTriggerEnter2D (Collider2D col)
    {
        if (col.CompareTag("PlayerBulletHitbox"))
        {
            SoundManagerScript.PlaySound("healthUp");
            GameManagerScript.health += 1;
        }
    }
}
