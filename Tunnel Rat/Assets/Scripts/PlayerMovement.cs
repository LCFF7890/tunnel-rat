﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public bool moving = false;
    public bool twoKeys = false;
    float speed = 5.0f;
    float speedCap = 1f;
    public Animator walkingAnimation;

    public static int daño;

    private Material matWhite;
    private Material matDefault;
    SpriteRenderer sr;


    // Start is called before the first frame update
    void Start()
    {

        sr = GetComponent<SpriteRenderer>();
        matWhite = Resources.Load("WhiteFlash", typeof(Material)) as Material;
        matDefault = sr.material;
        walkingAnimation = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {    
       movement();

    }

    private void movement()
    {
        if (twoKeys == true) { speedCap = 0.7f; } else { speedCap = 1f; }

        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.up * speed * speedCap* Time.deltaTime, Space.World);
            moving = true;
            //            walkingAnimation.SetBool("Moving", true);
            
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.down * speed * speedCap* Time.deltaTime, Space.World);
            moving = true;
            //            walkingAnimation.SetBool("Moving", true);
            
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left * speed * speedCap* Time.deltaTime, Space.World);
            moving = true;
           
            //            walkingAnimation.SetBool("Moving", true);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right * speed * speedCap* Time.deltaTime, Space.World);
            moving = true;
            
            //            walkingAnimation.SetBool("Moving", true);
        }
        if (Input.GetKey(KeyCode.D) != true && Input.GetKey(KeyCode.A) != true && Input.GetKey(KeyCode.S) != true && Input.GetKey(KeyCode.W) != true)
        {
            moving = false;
            twoKeys = false;
            //            walkingAnimation.SetBool("Moving", false);
        }

        if((Input.GetKey(KeyCode.W))&&(Input.GetKey(KeyCode.A))||
           (Input.GetKey(KeyCode.W))&&(Input.GetKey(KeyCode.D))||
           (Input.GetKey(KeyCode.S))&&(Input.GetKey(KeyCode.A))|| 
           (Input.GetKey(KeyCode.S))&&(Input.GetKey(KeyCode.D)))

        { twoKeys = true; }
        else { twoKeys = false; }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("EnemyBullet"))
        {
            daño++;
            sr.material = matWhite;
            Invoke("ResetMaterial", .1f);

        }

    }
    void ResetMaterial()
    {
        sr.material = matDefault;
    }

}