﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GotRifle : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerBulletHitbox"))
        {
            SoundManagerScript.PlaySound("changeWeapon");
            Spawner.haveRifle = true;
            Destroy(gameObject);

        }

    }
}
