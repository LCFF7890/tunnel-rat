﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LegAnimator : MonoBehaviour
{
 
    public Animator Legs;
    // Start is called before the first frame update
    void Start()
    {
        Legs = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.D)== true || Input.GetKey(KeyCode.A)== true || Input.GetKey(KeyCode.S)== true || Input.GetKey(KeyCode.W)== true)
        {
           
            Legs.SetBool("movimiento", true);
        }
        if (Input.GetKey(KeyCode.D) != true && Input.GetKey(KeyCode.A) != true && Input.GetKey(KeyCode.S) != true && Input.GetKey(KeyCode.W) != true)
        {
            Legs.SetBool("movimiento", false);
        }

    }
}

  
