﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HUDFollowPlayer : MonoBehaviour
{
    GameObject player;
    bool followPlayer = true;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("MainCamera");
    }


    // Update is called once per frame
    void Update()
    {
        if (followPlayer == true)
        {
            hudFollowPlayer();
        }
    }

    public void setFollowPlayer(bool val)
    {
        followPlayer = val;
    }

    void hudFollowPlayer()
    {
        Vector3 newPos = new Vector3(player.transform.position.x -2, player.transform.position.y+1, this.transform.position.z);
        this.transform.position = newPos;
    }
}
