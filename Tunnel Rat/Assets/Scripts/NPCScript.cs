﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCScript : MonoBehaviour
{
    public static bool speak1 = false;
    public GameObject dialogBox;


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerBulletHitbox"))
        {
            speak1 = true;
            StartCoroutine(DisableText(collision.gameObject));

        }
    }


    IEnumerator DisableText(GameObject text)
    {
        SoundManagerScript.PlaySound("militaryVoice");
        dialogBox.SetActive(true);


        yield return new WaitForSeconds(5.5f);

        dialogBox.SetActive(false);


    }

}
