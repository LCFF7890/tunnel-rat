﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class change_weapon : MonoBehaviour
{
    public Spawner weapon;

    public Sprite arms_player_machinegun;
    public Sprite arms_player_shotgun;
    public Sprite Holding_Gun_Sprite;


    public Spawner playerScript;
    // Start is called before the first frame update
    void Start()
    {
        GameObject thePlayer = GameObject.Find("Protagonista");
       playerScript = thePlayer.GetComponent<Spawner>();
    }

    // Update is called once per frame
    void Update()
    {
        

        switch (playerScript.weapon_suplementaria)
        {
            case 1:
                this.GetComponent<SpriteRenderer>().sprite = Holding_Gun_Sprite;
                Debug.Log("Arma: " + playerScript.weapon_suplementaria);
                break;
            case 2:
                this.GetComponent<SpriteRenderer>().sprite = arms_player_shotgun;
                Debug.Log("Arma: " + playerScript.weapon_suplementaria);
                break;
            case 3:
                this.GetComponent<SpriteRenderer>().sprite = arms_player_machinegun;
                Debug.Log("Arma: " + playerScript.weapon_suplementaria);
                break;
        }
    }
}
