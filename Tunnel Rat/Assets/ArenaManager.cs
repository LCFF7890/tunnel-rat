﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArenaManager : MonoBehaviour
{


    public GameObject EnterArenaBlock, LeaveArenaBlock;
    public GameObject releaseEnemyWall1, releaseEnemyWall2;
    public GameObject releaseRifleNPC;
    // Start is called before the first frame update
    void Start()
    {
        EnterArenaBlock.SetActive(false);
        LeaveArenaBlock.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {

        if (EnterArenaTriggerScript.laHePisado == true)
        {
            EnterArenaBlock.SetActive(true);
            releaseEnemyWall1.SetActive(false);
            releaseEnemyWall2.SetActive(false);

        }

        if (EnemyAI.arenaEnemiesKilled == 8)
        {
            LeaveArenaBlock.SetActive(false);
        }


        if (EnemyAI.arenaEnemiesKilled == 10)
        {
            releaseRifleNPC.SetActive(false);
        }
    }
}
