﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManagerScript : MonoBehaviour
{
    public static AudioClip gunShot;
    public static AudioClip gunHit;
    public static AudioClip soldierDie;
    public static AudioClip soldierShot;
    public static AudioClip healthPickUp;
    public static AudioClip heroHit;
    public static AudioClip allyVoice;
    public static AudioClip goSound;
    public static AudioClip shotgunShot;
    public static AudioClip changeWeapon;
    public static AudioClip rifleShot;
    public static AudioClip arenaAlarm;
    public static AudioClip needToReload;



    static AudioSource audioSrc;
    // Start is called before the first frame update
    void Start()
    {
        gunShot = Resources.Load<AudioClip>("playerShot");
        gunHit = Resources.Load<AudioClip>("enemyHit");
        soldierShot = Resources.Load<AudioClip>("enemyShot");
        soldierDie = Resources.Load<AudioClip>("enemyDie");
        healthPickUp = Resources.Load<AudioClip>("healthUp");
        heroHit = Resources.Load<AudioClip>("heroScream");
        allyVoice = Resources.Load<AudioClip>("militaryVoice");
        goSound = Resources.Load<AudioClip>("goSound");
        shotgunShot = Resources.Load<AudioClip>("shotgunShot");
        changeWeapon = Resources.Load<AudioClip>("changeWeapon");
        rifleShot = Resources.Load<AudioClip>("rifleShot");
        arenaAlarm = Resources.Load<AudioClip>("arenaAlarm");
        needToReload = Resources.Load<AudioClip>("needToReload");


        audioSrc = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public static void PlaySound (string clip)
    {
        switch (clip)
        {
            case "playerShot":
                audioSrc.PlayOneShot(gunShot);
                break;
            case "enemyHit":
                audioSrc.PlayOneShot(gunHit);
                break;
            case "enemyDie":
                audioSrc.PlayOneShot(soldierDie);
                break;
            case "enemyShot":
                audioSrc.PlayOneShot(soldierShot);
                break;
            case "healthUp":
                audioSrc.PlayOneShot(healthPickUp);
                break;
            case "heroScream":
                audioSrc.PlayOneShot(heroHit);
                break;
            case "militaryVoice":
                audioSrc.PlayOneShot(allyVoice);
                break;
            case "goSound":
                audioSrc.PlayOneShot(goSound);
                break;
            case "shotgunShot":
                audioSrc.PlayOneShot(shotgunShot);
                break;
            case "changeWeapon":
                audioSrc.PlayOneShot(changeWeapon);
                break;
            case "rifleShot":
                audioSrc.PlayOneShot(rifleShot);
                break;
            case "arenaAlarm":
                audioSrc.PlayOneShot(arenaAlarm);
                break;
            case "needToReload":
                audioSrc.PlayOneShot(needToReload);
                break;
        }
    }
    
}
